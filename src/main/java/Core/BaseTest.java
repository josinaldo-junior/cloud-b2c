package Core;

import Pages.LoginPage;
import org.junit.After;
import org.junit.Before;
import static Core.DriverFactory.killDriver;


public class BaseTest {

    private LoginPage page = new LoginPage();

    @Before
    public void Inicializa(){
        DriverFactory.getDriver().get("https://www.pneustore.com.br");

    }

    @After
    public void finaliza()  {
        if(Propriedades.FECHAR_BROWSER) {
            killDriver();
        }
    }

}
