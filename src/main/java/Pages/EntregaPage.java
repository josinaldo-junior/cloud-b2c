package Pages;

import Core.BasePage;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import static Core.DriverFactory.getDriver;

public class EntregaPage extends BasePage {

    public void clickTipoDeEntregueNormal(){
        WebDriverWait wait = new WebDriverWait(getDriver(), 20);
        wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector(".regular-options__list > .delivery-options-item > .items-center")));
        cliqueBotaoCSS(".regular-options__list > .delivery-options-item > .items-center");
    }

    public void clickTipoDeEntregaRetira(){

    }

    public void btnProximoEntrega() {
        WebDriverWait wait = new WebDriverWait(getDriver(), 20);
        wait.until(ExpectedConditions.elementToBeClickable(By.id("nextButtonSubmit")));
        cliqueBotao("nextButtonSubmit");
    }
}
