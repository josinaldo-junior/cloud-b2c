package SuiteTestes;


import Testes.*;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({
        CadastroTest.class,
        PrecoAVistaTest.class,
        BuscaPorMedidaTest.class,
        FluxoCompraTest.class,
        CarrinhoTest.class

})

public class SuiteTestes {
}
