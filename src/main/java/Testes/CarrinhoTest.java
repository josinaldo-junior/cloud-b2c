package Testes;

import Core.BaseTest;
import Pages.CarrinhoPage;
import Pages.HomePage;
import Pages.ItemPage;
import Pages.VitrinePage;
import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;

public class CarrinhoTest extends BaseTest {
    HomePage homePage = new HomePage();
    VitrinePage vitrinePage = new VitrinePage();
    ItemPage itemPage = new ItemPage();
    CarrinhoPage carrinhoPage = new CarrinhoPage();

    @Test
    public void validarQuantidadeComPneus() throws InterruptedException {
        /** 4 pneus com 6 unidades Total = 24 itens **/
        /** Pneu #1 **/
        homePage.barraDePesquisa("1010");
        Thread.sleep(1000);
        homePage.barraDePesquisa("0010");
        homePage.apertarEnter();
        homePage.clickLupa();
        Thread.sleep(1000);
        vitrinePage.clickItemCss();
        Thread.sleep(1000);
        itemPage.alterarQuantidadeDeItem();
        Thread.sleep(1000);
        itemPage.clickBtnComprar();
        Thread.sleep(1000);
        /** Pneu #2 **/
        homePage.barraDePesquisa("1007");
        Thread.sleep(1000);
        homePage.barraDePesquisa("0133");
        homePage.apertarEnter();
        homePage.clickLupa();
        Thread.sleep(1000);
        vitrinePage.clickItemCss();
        Thread.sleep(1000);
        itemPage.alterarQuantidadeDeItem();
        Thread.sleep(1000);
        itemPage.clickBtnComprar();
        Thread.sleep(1000);
        /** Pneu #3 **/
        homePage.barraDePesquisa("1007");
        Thread.sleep(1000);
        homePage.barraDePesquisa("0318");
        homePage.apertarEnter();
        homePage.clickLupa();
        Thread.sleep(1000);
        vitrinePage.clickItemCss();
        Thread.sleep(1000);
        itemPage.alterarQuantidadeDeItem();
        Thread.sleep(1000);
        itemPage.clickBtnComprar();
        Thread.sleep(1000);
        /** Pneu #4 **/
        homePage.barraDePesquisa("1007");
        Thread.sleep(1000);
        homePage.barraDePesquisa("0090");
        homePage.apertarEnter();
        homePage.clickLupa();
        Thread.sleep(1000);
        vitrinePage.clickItemCss();
        Thread.sleep(1000);
        itemPage.alterarQuantidadeDeItem();
        Thread.sleep(1000);
        itemPage.clickBtnComprar();
        Thread.sleep(1000);
        Assert.assertEquals("×\n" +
                "Opa! Você atingiu a quantidade máxima de produtos permitida.",carrinhoPage.mensagemDeItem());
    }

    @Test
    @Ignore
    public void validarQuantidadeComKits() throws InterruptedException {
        /** 1 kit **/
        homePage.barraDePesquisa("PI1007");
        Thread.sleep(1000);
        homePage.barraDePesquisa("0273");
        homePage.apertarEnter();
        homePage.clickLupa();
        Thread.sleep(1000);
        vitrinePage.clickItemCss();
        Thread.sleep(1000);
        itemPage.alterarQuantidadeDeItem();
        Thread.sleep(1000);
        itemPage.clickBtnComprar();
        Thread.sleep(1000);
        Assert.assertEquals("×\n" +
                "Opa! Você atingiu a quantidade máxima de produtos permitida.",carrinhoPage.mensagemDeItem());
    }

    @Test
    public void validarQuantidadeDeSKUs() throws InterruptedException {
        /** 1 sku **/
        homePage.barraDePesquisa("1007");
        Thread.sleep(1000);
        homePage.barraDePesquisa("0273");
        homePage.apertarEnter();
        homePage.clickLupa();
        Thread.sleep(1000);
        vitrinePage.ClickComprar();
        Thread.sleep(1000);
        /** 2 sku **/
        homePage.barraDePesquisa("1003");
        Thread.sleep(1000);
        homePage.barraDePesquisa("1439");
        homePage.apertarEnter();
        homePage.clickLupa();
        Thread.sleep(1000);
        vitrinePage.ClickComprar();
        Thread.sleep(1000);
        /** 3 sku **/
        homePage.barraDePesquisa("1013");
        Thread.sleep(1000);
        homePage.barraDePesquisa("0019");
        homePage.apertarEnter();
        homePage.clickLupa();
        Thread.sleep(1000);
        vitrinePage.ClickComprar();
        Thread.sleep(1000);
        /** 4 sku **/
        homePage.barraDePesquisa("1013");
        Thread.sleep(1000);
        homePage.barraDePesquisa("0069");
        homePage.apertarEnter();
        homePage.clickLupa();
        Thread.sleep(1000);
        vitrinePage.ClickComprar();
        Thread.sleep(1000);
        /** 5 sku **/
        homePage.barraDePesquisa("1007");
        Thread.sleep(1000);
        homePage.barraDePesquisa("0260");
        homePage.apertarEnter();
        homePage.clickLupa();
        Thread.sleep(1000);
        vitrinePage.ClickComprar();
        Thread.sleep(1000);
        /** 6 sku **/
        homePage.barraDePesquisa("1014");
        Thread.sleep(1000);
        homePage.barraDePesquisa("0090");
        homePage.apertarEnter();
        homePage.clickLupa();
        Thread.sleep(1000);
        vitrinePage.ClickComprar();
        Thread.sleep(1000);
        Assert.assertEquals("×\n" +
                "Opa! Você atingiu a quantidade máxima de produtos permitida.",carrinhoPage.mensagemDeItem());
    }
}