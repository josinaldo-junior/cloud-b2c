package Testes;

import Core.BaseTest;
import Pages.*;
import org.junit.Test;

public class FluxoCompraTest extends BaseTest {
    HomePage homePage = new HomePage();
    LoginPage loginPage = new LoginPage();
    VitrinePage vitrinePage = new VitrinePage();
    CarrinhoPage carrinhoPage = new CarrinhoPage();
    EnderecoPage enderecoPage = new EnderecoPage();
    EntregaPage entregaPage = new EntregaPage();
    PagamentoPage pagamentoPage = new PagamentoPage();

    @Test
    public void fluxoCompletoBoletoItemPorPesquisaNormal() throws Exception {
        Thread.sleep(2000);
        homePage.barraDePesquisa("7001");
        Thread.sleep(2000);
        homePage.barraDePesquisa("0121");
        Thread.sleep(2000);
        homePage.apertarEnter();
        Thread.sleep(2000);
        homePage.clickLupa();
        Thread.sleep(2000);
        vitrinePage.ClickComprar();
        Thread.sleep(2000);
        carrinhoPage.clickFinalizarCompra();
        loginPage.emailUsuarioAntigo();
        loginPage.senhaUsuarioAntigo();
        loginPage.btnEntrar();
        Thread.sleep(7000);
        enderecoPage.selecionarEndereco();
        Thread.sleep(7000);
        enderecoPage.btnProximoEndereco();
        Thread.sleep(7000);
        entregaPage.clickTipoDeEntregueNormal();
        Thread.sleep(7000);
        entregaPage.btnProximoEntrega();
        Thread.sleep(7000);
        pagamentoPage.PagBoleto();
        Thread.sleep(7000);
        pagamentoPage.FinalizaSuaCompra();
        Thread.sleep(7000);
        System.out.println(pagamentoPage.ObterNumeroPedido());
    }

    @Test
    public void fluxoCompletoPixItemPorPesquisaNormal() throws Exception {
        Thread.sleep(2000);
        homePage.barraDePesquisa("7001");
        Thread.sleep(2000);
        homePage.barraDePesquisa("0121");
        Thread.sleep(2000);
        homePage.apertarEnter();
        Thread.sleep(2000);
        homePage.clickLupa();
        Thread.sleep(2000);
        vitrinePage.ClickComprar();
        Thread.sleep(2000);
        carrinhoPage.clickFinalizarCompra();
        loginPage.emailUsuarioAntigo();
        loginPage.senhaUsuarioAntigo();
        loginPage.btnEntrar();
        Thread.sleep(7000);
        enderecoPage.selecionarEndereco();
        Thread.sleep(7000);
        enderecoPage.btnProximoEndereco();
        Thread.sleep(7000);
        entregaPage.clickTipoDeEntregueNormal();
        Thread.sleep(7000);
        entregaPage.btnProximoEntrega();
        Thread.sleep(7000);
        pagamentoPage.PagPix();
        Thread.sleep(7000);
        pagamentoPage.FinalizaSuaCompra();
        Thread.sleep(7000);
        System.out.println(pagamentoPage.ObterNumeroPedido());
    }



}
