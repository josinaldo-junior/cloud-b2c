package Testes;

import Core.BaseTest;
import Pages.CarrinhoPage;
import Pages.HomePage;
import Pages.ItemPage;
import Pages.VitrinePage;
import org.junit.Assert;
import org.junit.Test;


public class PrecoAVistaTest extends BaseTest {
    private HomePage homePage = new HomePage();
    private VitrinePage vitrinePage = new VitrinePage();
    private ItemPage itemPage = new ItemPage();
    private CarrinhoPage carrinhoPage = new CarrinhoPage();

    @Test
    public void testBuscarPorItemPirelli() throws InterruptedException {

        homePage.barraDePesquisa("1007");
        Thread.sleep(2000);
        homePage.barraDePesquisa("0090");
        homePage.apertarEnter();
        homePage.clickLupa();
        Assert.assertEquals("R$ 599,00",vitrinePage.vitrineObterPrecoAVista());
        Assert.assertEquals("ou 6x de R$ 99,83",vitrinePage.vitrineObterPrecoParcelado());
        vitrinePage.clickItemXpath();
        Assert.assertEquals("R$ 599,00",itemPage.obterPrecoAVistaItem());
        Assert.assertEquals("em até 6x de R$ 99,83 sem juros",itemPage.obterPrecoParceladoItem());
    }

    @Test
    public void testBuscarPorItemContinental() throws InterruptedException {
        homePage.barraDePesquisa("1012");
        Thread.sleep(2000);
        homePage.barraDePesquisa("0212");
        homePage.clickLupa();
        Assert.assertEquals("R$ 819,90",vitrinePage.vitrineObterPrecoAVista());
        Assert.assertEquals("ou 6x de R$ 151,83",vitrinePage.vitrineObterPrecoParcelado());
        vitrinePage.clickItemXpath();
        Assert.assertEquals("R$ 819,90à vista no cartão de débito ou via PIX",itemPage.obterPrecoAVistaItem());
        Assert.assertEquals("ou R$ 911,00 em até 6x de R$ 151,83 sem juros\n" +
                "Veja mais opções de parcelamento clicando aqui!",itemPage.obterPrecoParceladoItem());
    }


    @Test
    public void testBuscarPorItemAptany() throws InterruptedException {
        homePage.barraDePesquisa("1003");
        Thread.sleep(2000);
        homePage.barraDePesquisa("0639");
        homePage.clickLupa();
//      Assert.assertEquals("Pneu Aptany Aro 16 RP203 215/65R16 98H",vitrinePage.vitrineObterTitulo());
        Assert.assertEquals("R$ 277,56",vitrinePage.vitrineObterPrecoAVista());
        Assert.assertEquals("ou 6x de R$ 51,40",vitrinePage.vitrineObterPrecoParcelado());
        vitrinePage.clickItemXpath();
        Assert.assertEquals("R$ 277,56à vista no cartão de débito ou via PIX",itemPage.obterPrecoAVistaItem());
        Assert.assertEquals("ou R$ 308,40 em até 6x de R$ 51,40 sem juros\n" +
                "Veja mais opções de parcelamento clicando aqui!",itemPage.obterPrecoParceladoItem());
//       itemPage.clickBtnComprar();
//       Assert.assertEquals("R$ 398,83",carrinhoPage.verificarSubTotal());
    }

}


